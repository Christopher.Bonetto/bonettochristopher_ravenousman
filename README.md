# BonettoChristopher_RavenousMan

Unreal Engine 4 and C++

Unreal version:
4.25.4

A simple game where the player has to reach the end of the level.
There are invisible enemies placed on the level and the player can survive using rifles and health globes.
The player has to think before shoot because he has a certain amount of bullets (he can find new ones on the level).
