// Copyright Epic Games, Inc. All Rights Reserved.

#define print(text) if(GEngine) GEngine->AddOnScreenDebugMessage(-1,1.5f,FColor::Green,text);

#include "RavenousManCharacter.h"

#include "DrawDebugHelpers.h"
#include "RavenousManProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "PickableItem.h"
#include "Chaos/GeometryParticlesfwd.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ARavenousManCharacter

ARavenousManCharacter::ARavenousManCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	MaxAmmo = 50;
	hasWeapon = false;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	SK_Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	SK_Mesh->SetOnlyOwnerSee(true);
	SK_Mesh->SetupAttachment(FirstPersonCameraComponent);
	SK_Mesh->bCastDynamicShadow = false;
	SK_Mesh->CastShadow = false;
	SK_Mesh->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	SK_Mesh->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a gun mesh component
	SM_Weapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FP_Gun"));
	SM_Weapon->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	SM_Weapon->bCastDynamicShadow = false;
	SM_Weapon->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	SM_Weapon->SetupAttachment(SK_Mesh, TEXT("GripPoint"));

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(SM_Weapon);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

}



//////////////////////////////////////////////////////////////////////////
// Input

void ARavenousManCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ARavenousManCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ARavenousManCharacter::StopFire);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ARavenousManCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARavenousManCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ARavenousManCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ARavenousManCharacter::LookUpAtRate);
}

void ARavenousManCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	
	CurrentAmmo = MaxAmmo;

	print("Star play");

	if(PlayerWidgetClass != nullptr)
	{
		PlayerWidget = CreateWidget(GetWorld(), PlayerWidgetClass);
		PlayerWidget->AddToViewport();
	}
	
	SM_Weapon->AttachToComponent(SK_Mesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
}

void ARavenousManCharacter::Tick(float DeltaSeconds)
{
	// if(!hasWeapon) return;
	// if(CurrentAmmo <= 0) return;
	// if(isFireButtonPressed && GetWorld()->TimeSeconds - LastShootTime >= TimeBetweenShots)
	// {
	// 	FireShot();
	// 	LastShootTime = GetWorld()->TimeSeconds;
	// }
}

void ARavenousManCharacter::StartFire()
{
	if(!hasWeapon || CurrentAmmo <= 0) return; 
	
	isFireButtonPressed = true;

	if(GetWorld()->TimeSeconds - LastShootTime >= TimeBetweenShots)
	{
		FireShot();
		LastShootTime = GetWorld()->TimeSeconds;
	}
}

void ARavenousManCharacter::StopFire()
{
	isFireButtonPressed = false;
}

void ARavenousManCharacter::FireShot()
{
	CurrentAmmo--;
	
	FHitResult Hit;

	const float WeaponRange = 20000.f;

	const FVector StartTrace = FirstPersonCameraComponent->GetComponentLocation();
	const FVector EndTrace = (FirstPersonCameraComponent->GetForwardVector() * WeaponRange) + StartTrace;

	FCollisionQueryParams QueryParams = FCollisionQueryParams(SCENE_QUERY_STAT(WeaponTrace),true,this);

	if(GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Visibility, QueryParams))
	{
		if(Hit.GetActor()->GetClass()->ImplementsInterface(UI_HpInterface::StaticClass()))
		{
			II_HpInterface::Execute_SetNewHealthValue(Hit.GetActor(), -WeaponDamage);
		}

		if(ImpactParticles)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint));
		}
	}

	if(MuzzleParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MuzzleParticles, SM_Weapon->GetSocketTransform(FName("Muzzle")));
	}
	
	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = SK_Mesh->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}

}

void ARavenousManCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ARavenousManCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ARavenousManCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ARavenousManCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

// void AEntityCharacter::OnBeginOverlap(UPrimitiveComponent* HitComp,
//                                         AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
//                                         bool bFromSweep, const FHitResult& SweepResult)
// {
// 	UE_LOG(LogTemp, Warning, TEXT("Text, %d"), OtherActor->GetName() );
// 	APickableItem* item = Cast<APickableItem>(OtherActor);
//
// 	item->PickItem();
//                                     
// 	OtherActor->Destroy();
// 	
// 	//UE_LOG(LogTemp, Warning, TEXT("Collided with s%"), OtherActor->GetName());
// 	if(OtherActor->ActorHasTag("Pickable"))
// 	{
// 		
// 	}
//                                     
// }

