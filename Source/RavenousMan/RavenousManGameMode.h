// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RavenousManGameMode.generated.h"

UCLASS(minimalapi)
class ARavenousManGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARavenousManGameMode();
};



