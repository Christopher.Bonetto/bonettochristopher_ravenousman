// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"

#include "Math/UnrealMathUtility.h"

#include "EntityCharacter.generated.h"

UCLASS()
class RAVENOUSMAN_API AEntityCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Mesh)
	class USkeletalMeshComponent* SK_Mesh;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
	class UStaticMeshComponent* SM_Weapon;
	
public:

	// UFUNCTION()
 //            void OnBeginOverlap(class UPrimitiveComponent* HitComp,
 //                class AActor* OtherActor, class UPrimitiveComponent* OtherComp,
 //                int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	
public:
	// Sets default values for this character's properties
	AEntityCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
