// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class RavenousMan : ModuleRules
{
	public RavenousMan(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
