// Copyright Epic Games, Inc. All Rights Reserved.

#include "RavenousManGameMode.h"
#include "RavenousManHUD.h"
#include "RavenousManCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARavenousManGameMode::ARavenousManGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	//DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ARavenousManHUD::StaticClass();
}
